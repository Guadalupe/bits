

function unsetBits(num){

	var bits = num.toString(2);

	var numUnsetBits = 0;

	for(i in bits){

		if(bits[i] == 0){

			numUnsetBits++;

		}

	}

	numUnsetBits = numUnsetBits + (16 - bits.length);

	return numUnsetBits;

}

console.log(unsetBits(65535));
console.log(unsetBits(12547));